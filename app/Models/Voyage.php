<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voyage extends Model
{
	use HasFactory;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		"started_at",
		"ended_at",
		"revenues",
		"expenses",
		"vessel_id",
		"status",
		"code",
		"profit"
	];

	/**
	 * The attributes that should be cast.
	 *
	 * @var array
	 */
	protected $casts = [
		'started_at' => 'datetime',
		'ended_at' => 'datetime',
	];

	public function vessel()
	{
		return $this->belongsTo(Vessel::class);
	}
}
