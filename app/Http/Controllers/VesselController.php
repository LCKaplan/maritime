<?php

namespace App\Http\Controllers;

use App\Http\Resources\VesselResource;
use App\Models\Vessel;
use App\Services\Vessel\VesselService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Response as FacadesResponse;

class VesselController extends Controller
{

	/**      
	 * @var VesselService   
	 */
	protected $vesselService;

	/**      
	 * VesselController constructor.      
	 *      
	 * @param VesselService $model      
	 */
	public function __construct(VesselService $vesselService)
	{
		$this->vesselService = $vesselService;
	}

	/**
	 * Store a new vessel.
	 *
	 * @param  Request  $request
	 * @return VesselResource
	 */
	public function store(Request $request)
	{
		$vessel = $this->vesselService->createVessel($request->all());

		return new VesselResource($vessel);
	}

	public function financialReport(Vessel $vessel)
	{
		$voyages = $vessel->voyages;

		$voyages = $voyages->map(function ($voyage) {
			return [
				'voyage_id' => $voyage->id,
				'start' => $voyage->started_at->format('Y-m-d H:i:s'),
				'end' => $voyage->ended_at->format('Y-m-d H:i:s'),
				"voyage_revenues" => $voyage->revenues,
				"voyage_expenses" => $voyage->expenses,
				"voyage_profit" => $voyage->profit,
				"voyage_profit_daily_average" => $voyage->profit / $voyage->started_at->diffInDays($voyage->ended_at),
			];
		});

		return FacadesResponse::json([
			'data' => $voyages
		]);
	}
}
