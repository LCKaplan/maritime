<?php

namespace App\Http\Controllers;

use App\Http\Resources\VoyageResource;
use App\Models\Voyage;
use App\Services\Voyage\VoyageService;
use Illuminate\Http\Request;

class VoyageController extends Controller
{
	/**      
	 * @var VesselService   
	 */
	protected $voyageService;

	/**      
	 * VoyageController constructor.      
	 *      
	 * @param VoyageService $model      
	 */
	public function __construct(VoyageService $voyageService)
	{
		$this->voyageService = $voyageService;
	}

	/**
	 * List all voyages
	 * 
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function index()
	{
		$voyages = $this->voyageService->getAllVoyages();

		return VoyageResource::collection($voyages);
	}

	/**
	 * Store a new voyage.
	 *
	 * @param  Request  $request
	 * @return VoyageResource
	 */
	public function store(Request $request)
	{
		$voyage = $this->voyageService->createVoyage($request->all());

		return new VoyageResource($voyage);
	}

	public function update(Request $request, Voyage $voyage)
	{
		$voyage = $this->voyageService->updateVoyageById($voyage->id, $request->all());

		return new VoyageResource($voyage);
	}
}
