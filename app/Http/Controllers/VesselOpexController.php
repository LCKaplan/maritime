<?php

namespace App\Http\Controllers;

use App\Http\Resources\VesselOpexResource;
use App\Models\Vessel;
use App\Services\VesselOpex\VesselOpexService;
use Illuminate\Http\Request;

class VesselOpexController extends Controller
{
	/**      
	 * @var VesselOpexService   
	 */
	protected $vesselOpexService;

	/**      
	 * VesselOpexController constructor.      
	 *      
	 * @param VesselOpexService $model      
	 */
	public function __construct(VesselOpexService $vesselOpexService)
	{
		$this->vesselOpexService = $vesselOpexService;
	}

	/**
	 * Store a new vesselOpex.
	 *
	 * @param  Request  $request
	 * @param  Vessel  $vessel
	 * @return VesselOpexResource
	 */
	public function store(Request $request, Vessel $vessel)
	{
		$vesselOpex = $this->vesselOpexService->createVesselOpex($request->all(), $vessel);

		return new VesselOpexResource($vesselOpex);
	}
}
