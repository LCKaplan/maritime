<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VoyageResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'code' => $this->code,
			'vessel_id' => $this->vessel_id,
			'status' => $this->status,
			'started_at' => $this->started_at,
			'ended_at' => $this->ended_at,
			'revenues' => $this->revenues,
			'expenses' => $this->expenses,
			'profit' => $this->profit,
			'revenues' => $this->revenues,
		];
	}
}
