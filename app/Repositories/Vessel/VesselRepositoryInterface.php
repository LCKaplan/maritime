<?php

namespace App\Repositories\Vessel;

use App\Models\Vessel;

interface VesselRepositoryInterface
{
	/**
	 * @param array $attributes
	 * @return Vessel
	 */
	public function create(array $attributes): Vessel;
}
