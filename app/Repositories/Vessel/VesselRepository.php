<?php

namespace App\Repositories\Vessel;

use App\Models\Vessel;

class VesselRepository implements VesselRepositoryInterface
{

	/**      
	 * @var Vessel      
	 */
	protected $vessel;

	/**      
	 * VesselRepository constructor.      
	 *      
	 * @param Vessel $model      
	 */
	public function __construct(Vessel $vessel)
	{
		$this->vessel = $vessel;
	}

	/**
	 * @param array $attributes
	 *
	 * @return Vessel
	 */
	public function create(array $attributes): Vessel
	{
		return $this->vessel->create($attributes);
	}
}
