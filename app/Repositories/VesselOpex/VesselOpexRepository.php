<?php

namespace App\Repositories\VesselOpex;

use App\Models\VesselOpex;

class VesselOpexRepository implements VesselOpexRepositoryInterface
{

	/**      
	 * @var VesselOpex      
	 */
	protected $vesselOpex;

	/**      
	 * VesselOpexRepository constructor.      
	 *      
	 * @param VesselOpex $model      
	 */
	public function __construct(VesselOpex $vesselOpex)
	{
		$this->vesselOpex = $vesselOpex;
	}

	/**
	 * @param array $attributes
	 *
	 * @return VesselOpex
	 */
	public function create(array $attributes): VesselOpex
	{
		return $this->vesselOpex->create($attributes);
	}
}
