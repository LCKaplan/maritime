<?php

namespace App\Repositories\VesselOpex;

use App\Models\VesselOpex;

interface VesselOpexRepositoryInterface
{
	/**
	 * @param array $attributes
	 * @return VesselOpex
	 */
	public function create(array $attributes): VesselOpex;
}
