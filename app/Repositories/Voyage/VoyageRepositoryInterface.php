<?php

namespace App\Repositories\Voyage;

use App\Models\Voyage;

interface VoyageRepositoryInterface
{
	/**
	 * @param array $attributes
	 * @return Voyage
	 */
	public function create(array $attributes): Voyage;

	/**
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getAllVoyages(): \Illuminate\Database\Eloquent\Collection;

	/**
	 * @param int $id
	 * @param array $attributes
	 * @return Voyage
	 */
	public function updateById(int $id, array $attributes): Voyage;
}
