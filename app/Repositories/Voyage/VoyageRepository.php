<?php

namespace App\Repositories\Voyage;

use App\Models\Voyage;

class VoyageRepository implements VoyageRepositoryInterface
{

	/**      
	 * @var Voyage      
	 */
	protected $voyage;

	/**      
	 * VoyageRepository constructor.      
	 *      
	 * @param Voyage $model      
	 */
	public function __construct(Voyage $voyage)
	{
		$this->voyage = $voyage;
	}

	/**
	 * Create a new voyage
	 * 
	 * @param array $attributes
	 *
	 * @return Voyage
	 */
	public function create(array $attributes): Voyage
	{
		return $this->voyage->create($attributes);
	}

	/**
	 * Get all voyages
	 * 
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getAllVoyages(): \Illuminate\Database\Eloquent\Collection
	{
		return $this->voyage->all();
	}

	/**
	 * Update a new voyage
	 * 
	 * @param int $id
	 * @param array $attributes
	 *
	 * @return Voyage
	 */
	public function updateById(int $id, array $attributes): Voyage
	{
		$voyage = Voyage::findOrFail($id);
		$voyage->update($attributes);

		return $voyage;
	}
}
