<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Decimal implements Rule
{
	protected $total;
	protected $places;

	/**
	 * Create a new rule instance.
	 *
	 * @return void
	 */
	public function __construct(int $total, int $places = 2)
	{
		$this->total = $total;
		$this->places = $places;
	}

	/**
	 * Determine if the validation rule passes.
	 *
	 * @param  string  $attribute
	 * @param  mixed  $value
	 * @return bool
	 */
	public function passes($attribute, $value)
	{
		return preg_match("/^[0-9]{1,{$this->total}}(\.[0-9]{1,{$this->places}})$/", $value);
	}

	/**
	 * Get the validation error message.
	 *
	 * @return string
	 */
	public function message()
	{
		return 'The :attribute must be an appropriately formatted decimal e.g. ' . $this->example();
	}

	/**
	 * Get an example formated decimal
	 */
	protected function example()
	{
		return mt_rand(1, (int) str_repeat('9', $this->total)) .
			'.' .
			mt_rand(1, (int) str_repeat('9', $this->places));
	}
}
