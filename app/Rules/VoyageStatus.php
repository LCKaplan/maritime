<?php

namespace App\Rules;

use App\Models\Vessel;
use App\Models\Voyage;
use Illuminate\Contracts\Validation\Rule;

class VoyageStatus implements Rule
{
	protected $errorMessage;
	protected $voyage;
	/**
	 * Create a new rule instance.
	 *
	 * @param Voyage $voyage
	 * @return void
	 */
	public function __construct(Voyage $voyage)
	{
		$this->voyage = $voyage;
	}

	/**
	 * Determine if the validation rule passes.
	 *
	 * @param  string  $attribute
	 * @param  mixed  $value
	 * @return bool
	 */
	public function passes($attribute, $value)
	{
		if ($value == 'ongoing') {
			$ongoingVoyageExists = $this->voyage->vessel->voyages()
				->where('id', '<>', $this->voyage->id)
				->where('status', 'ongoing')->exists();

			if ($ongoingVoyageExists) {
				$this->errorMessage = 'There is another ongoing voyage for the same vessel!';
				return false;
			}
		}

		if ($value == 'submitted') {
			$requiredFields = [
				'started_at',
				'ended_at',
				'revenues',
				'expenses',
			];

			foreach ($requiredFields as $field) {
				if (!request($field)) {
					$this->errorMessage = $field . ' field is required to submit a voyage!';
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Get the validation error message.
	 *
	 * @return string
	 */
	public function message()
	{
		return $this->errorMessage;
	}
}
