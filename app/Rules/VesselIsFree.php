<?php

namespace App\Rules;

use App\Models\Vessel;
use Illuminate\Contracts\Validation\Rule;

class VesselIsFree implements Rule
{

	protected $voyageId;

	/**
	 * Create a new rule instance.
	 *
	 * @param  int $voyageId
	 * @return void
	 */
	public function __construct(int $voyageId = 0)
	{
		$this->voyageId == $voyageId;
	}

	/**
	 * Determine if the validation rule passes.
	 *
	 * @param  string  $attribute
	 * @param  mixed  $value
	 * @return bool
	 */
	public function passes($attribute, $value)
	{
		$vessel = Vessel::findOrFail($value);

		return $vessel->voyages()
			->where('id', '<>', $this->voyageId)
			->where(function ($query) {
				$query->where('started_at', '<=', request('started_at'))
					->where('ended_at', '>=', request('started_at'));
				if (request('ended_at')) {
					$query->orWhere(function ($q) {
						$q->where('started_at', '<=', request('ended_at'))
							->where('ended_at', '>=', request('ended_at'));
					});
				}
			})->doesntExist();
	}

	/**
	 * Get the validation error message.
	 *
	 * @return string
	 */
	public function message()
	{
		return 'Vessel is not available this voyage.';
	}
}
