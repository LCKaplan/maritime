<?php

namespace App\Services\Vessel;

use App\Models\Vessel;
use App\Repositories\Vessel\VesselRepository;
use Illuminate\Support\Facades\Validator;

class VesselService implements VesselServiceInterface
{

	/**      
	 * @var VesselRepository      
	 */
	protected $vesselRepository;

	/**      
	 * VesselService constructor.      
	 *      
	 * @param Vessel $model      
	 */
	public function __construct(VesselRepository $vesselRepository)
	{
		$this->vesselRepository = $vesselRepository;
	}

	/**
	 * @param array $attributes
	 *
	 * @return Vessel
	 */
	public function createVessel(array $attributes): Vessel
	{
		Validator::validate($attributes, [
			'name' => 'required|string',
			'imo_number' => 'required|string'
		]);

		return $this->vesselRepository->create($attributes);
	}
}
