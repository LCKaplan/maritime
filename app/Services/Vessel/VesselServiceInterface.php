<?php

namespace App\Services\Vessel;

use App\Models\Vessel;

interface VesselServiceInterface
{
	/**
	 * @param array $attributes
	 * @return Vessel
	 */
	public function createVessel(array $attributes): Vessel;
}
