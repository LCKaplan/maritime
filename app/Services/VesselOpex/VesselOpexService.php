<?php

namespace App\Services\VesselOpex;

use App\Models\Vessel;
use App\Models\VesselOpex;
use App\Repositories\VesselOpex\VesselOpexRepository;
use App\Rules\Decimal;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class VesselOpexService implements VesselOpexServiceInterface
{

	/**      
	 * @var VesselOpexRepository      
	 */
	protected $vesselOpexRepository;

	/**      
	 * VesselOpexService constructor.      
	 *      
	 * @param VesselOpex $model      
	 */
	public function __construct(VesselOpexRepository $vesselOpexRepository)
	{
		$this->vesselOpexRepository = $vesselOpexRepository;
	}

	/**
	 * @param array $attributes
	 * @param Vessel $vessel
	 *
	 * @return VesselOpex
	 */
	public function createVesselOpex(array $attributes, Vessel $vessel): VesselOpex
	{
		Validator::validate($attributes, [
			'date' => [
				'required',
				'date',
				'date_format:Y-m-d',
				Rule::unique('vessel_opexes')->where(function ($query) use ($vessel) {
					return $query->where('vessel_id', $vessel->id)
						->where('date', request('date'));
				})
			],
			'expenses' => ['required', new Decimal(8, 2)],
		]);

		$attributes['vessel_id'] = $vessel->id;

		return $this->vesselOpexRepository->create($attributes);
	}
}
