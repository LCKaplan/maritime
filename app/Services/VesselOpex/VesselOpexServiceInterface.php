<?php

namespace App\Services\VesselOpex;

use App\Models\Vessel;
use App\Models\VesselOpex;

interface VesselOpexServiceInterface
{
	/**
	 * @param array $attributes
	 * @return VesselOpex
	 * @param Vessel $vessel
	 */
	public function createVesselOpex(array $attributes, Vessel $vessel): VesselOpex;
}
