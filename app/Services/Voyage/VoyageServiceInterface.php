<?php

namespace App\Services\Voyage;

use App\Models\Voyage;

interface VoyageServiceInterface
{
	/**
	 * @param array $attributes
	 * @return Voyage
	 */
	public function createVoyage(array $attributes): Voyage;

	/**
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getAllVoyages(): \Illuminate\Database\Eloquent\Collection;

	/**
	 * @param int $id
	 * @param array $attributes
	 * @return Voyage
	 */
	public function updateVoyageById(int $id, array $attributes): Voyage;
}
