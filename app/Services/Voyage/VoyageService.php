<?php

namespace App\Services\Voyage;

use App\Models\Vessel;
use App\Models\Voyage;
use App\Repositories\Voyage\VoyageRepository;
use App\Rules\Decimal;
use App\Rules\VesselIsFree;
use App\Rules\VoyageStatus;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class VoyageService implements VoyageServiceInterface
{

	/**      
	 * @var VoyageRepository      
	 */
	protected $voyageRepository;

	/**      
	 * VoyageService constructor.      
	 *      
	 * @param Voyage $model      
	 */
	public function __construct(VoyageRepository $voyageRepository)
	{
		$this->voyageRepository = $voyageRepository;
	}

	/**
	 * List all voyages
	 * 
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getAllVoyages(): \Illuminate\Database\Eloquent\Collection
	{
		return $this->voyageRepository->getAllVoyages();
	}

	/**
	 * @param array $attributes
	 *
	 * @return Voyage
	 */
	public function createVoyage(array $attributes): Voyage
	{
		Validator::validate($attributes, [
			'started_at' => 'required|date|date_format:Y-m-d H:i:s',
			'ended_at' => 'nullable|date|date_format:Y-m-d H:i:s|after:started_at',
			'revenues' => ['nullable', new Decimal(8, 2)],
			'expenses' => ['nullable', new Decimal(8, 2)],
			'vessel_id' => ['required', 'exists:vessels,id', new VesselIsFree()]
		]);

		$vessel = Vessel::findOrFail($attributes['vessel_id']);

		$attributes['status'] = 'pending';
		$attributes['code'] = $vessel->name . "-" . Carbon::parse($attributes['started_at'])->format('Y-m-d');
		$attributes['profit'] = ($attributes['revenues'] ?? 0) - ($attributes['expenses'] ?? 0);

		return $this->voyageRepository->create($attributes);
	}

	/**
	 * @param int $id
	 * @param array $attributes
	 * @return Voyage
	 */
	public function updateVoyageById($id, $attributes): Voyage
	{
		$voyage = Voyage::findOrFail($id);
		$vessel = $voyage->vessel;

		Validator::validate($attributes, [
			'started_at' => 'required|date|date_format:Y-m-d H:i:s',
			'ended_at' => 'nullable|date|date_format:Y-m-d H:i:s|after:started_at',
			'revenues' => ['nullable', new Decimal(8, 2)],
			'expenses' => ['nullable', new Decimal(8, 2)],
			'status' => ['required', 'in:pending,ongoing,submitted', new VoyageStatus($voyage)],
		]);

		$attributes['code'] = $vessel->name . "-" . Carbon::parse($attributes['started_at'])->format('Y-m-d');
		$attributes['profit'] = ($attributes['revenues'] ?? 0) - ($attributes['expenses'] ?? 0);

		return $this->voyageRepository->updateById($id, $attributes);
	}
}
