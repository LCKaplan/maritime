<?php

use App\Http\Controllers\VesselController;
use App\Http\Controllers\VesselOpexController;
use App\Http\Controllers\VoyageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('vessels/{vessel}/vessel-opex', VesselOpexController::class)->only('store');
Route::get('vessels/{vessel}/financial-report', [VesselController::class, 'financialReport']);
Route::apiResource('vessels', VesselController::class)->only('store');
Route::apiResource('voyages', VoyageController::class)->only(['index', 'store', 'update']);
