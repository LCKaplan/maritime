<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoyagesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('voyages', function (Blueprint $table) {
			$table->id();
			$table->foreignId('vessel_id');
			$table->string('code');
			$table->dateTime('started_at');
			$table->dateTime('ended_at')->nullable();
			$table->string('status');
			$table->decimal('revenues', 8, 2)->nullable();
			$table->decimal('expenses', 8, 2)->nullable();
			$table->decimal('profit', 8, 2)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('voyages');
	}
}
